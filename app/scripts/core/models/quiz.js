/**
 * Model
 */
/*global App*/
'use strict';

var _ = require('underscore'),
	Backbone = require('backbone');

module.exports = Backbone.Model.extend({
	url: '/api/question',

	initialize: function () {}
});