/*
 * List
 * http://labs.vertic.com
 *
 * Copyright 2015, Vertic A/S
 *
 * @author rkalms <rkalms@vertic.com>
 *
 *
 * NAMING CONVENTIONS:
 * -------------------
 * Singleton-literals and prototype objects:  PascalCase
 *
 * Functions and public variables:            camelCase
 *
 * Global variables and constants:            UPPERCASE
 *
 * Private variables:                         _underscorePrefix
 */

 /*jslint plusplus: true, vars: true, browser: true, white:true*/
/*global require, App*/

'use strict';

var $ = require('jquery'),
	_ = require('underscore'),
	Core = require('vertic-core-library'),
	Backbone = require('backbone'),
	Marionette = require('marionette'),
	Handlebars = require('handlebars'),

	// Views
	ItemView = require('./item');

var View = Marionette.CollectionView.extend({
	tagName: 'ul',

	className: 'quiz__list',

	childView: ItemView,

	initialize: function () {
		if (!App.admin) {
			App.socket.on('flip', function (index) {
				this.flip(index);
			}.bind(this));
		}
	},

	onShow: function () {},

	/**
	 * Flip handler
	 * 
	 * @param  {Number} index
	 */
	flip: function (index) {
		this.$list = this.$el.find('.quiz__list__item');

		var $element = this.$el.find('.quiz__list__item').eq(index);

		if (!$element.hasClass('flip')) {
			$element.addClass('flip');
		} else {
			$element.removeClass('flip');
		}
	}
});

module.exports = View;