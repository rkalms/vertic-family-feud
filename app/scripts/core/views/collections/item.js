/*
 * Item
 * http://labs.vertic.com
 *
 * Copyright 2015, Vertic A/S
 *
 * @author rkalms <rkalms@vertic.com>
 *
 *
 * NAMING CONVENTIONS:
 * -------------------
 * Singleton-literals and prototype objects:  PascalCase
 *
 * Functions and public variables:            camelCase
 *
 * Global variables and constants:            UPPERCASE
 *
 * Private variables:                         _underscorePrefix
 */

 /*jslint plusplus: true, vars: true, browser: true, white:true*/
/*global require, App*/

'use strict';

var $ = require('jquery'),
	_ = require('underscore'),
	Backbone = require('backbone'),
	Marionette = require('marionette'),
	Handlebars = require('handlebars'),

	// Template
	template = require('../../templates/item-template.html');

var View = Marionette.ItemView.extend({
	tagName: 'li',

	events: {
		'click': '_handleFlip',
		'touchstart': '_handleFlip',
	},

	className: 'quiz__list__item',

	template: Handlebars.compile(template),

	_handleFlip: function(event) {
		var $element = $(event.target).closest('.quiz__list__item');

		var amount = this.model.get('amount');

		App.currentScore = Number(App.currentScore) + Number(amount);

		if (!$element.hasClass('flip')) {
			$element.addClass('flip');
		} else {
			$element.removeClass('flip');
		}

		App.socket.emit('flip', {
			'index': $element.index()
		});
	}
});

module.exports = View;
