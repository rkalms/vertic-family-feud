/*
 * Question
 * http://labs.vertic.com
 *
 * Copyright 2015, Vertic A/S
 *
 * @author rkalms <rkalms@vertic.com>
 *
 *
 * NAMING CONVENTIONS:
 * -------------------
 * Singleton-literals and prototype objects:  PascalCase
 *
 * Functions and public variables:            camelCase
 *
 * Global variables and constants:            UPPERCASE
 *
 * Private variables:                         _underscorePrefix
 */

 /*jslint plusplus: true, vars: true, browser: true, white:true*/
/*global require: true*/

'use strict';

var $ = require('jquery'),
	_ = require('underscore'),
	Backbone = require('backbone'),
	Marionette = require('marionette'),
	Handlebars = require('handlebars'),

	// Template
	template = require('../templates/question-template.html');

var View = Marionette.ItemView.extend({
	tagName: 'h1',

	className: 'quiz__question__headline',

	template: Handlebars.compile(template)
});

module.exports = View;
