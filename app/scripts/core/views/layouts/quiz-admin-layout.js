/*
 * AppLayout
 * http://labs.vertic.com
 *
 * Copyright 2015, Vertic A/S
 *
 * @author rkalms <rkalms@vertic.com>
 *
 *
 * NAMING CONVENTIONS:
 * -------------------
 * Singleton-literals and prototype objects:  PascalCase
 *
 * Functions and public variables:            camelCase
 *
 * Global variables and constants:            UPPERCASE
 *
 * Private variables:                         _underscorePrefix
 */

 /*jslint plusplus: true, vars: true, browser: true, white:true*/
/*global require, App*/

'use strict';

var $ = require('jquery'),
	_ = require('underscore'),
	Backbone = require('backbone'),
	Marionette = require('marionette'),
	Handlebars = require('handlebars'),

	template = require('../../templates/quiz-admin-template.html'),

	// Views
	ListView = require('../collections/list'),
	QuestionView = require('../question');

var Layout = Marionette.LayoutView.extend({
	template: Handlebars.compile(template),

	events: {
		'click .admin__begin': '_begin',
		'touchstart .admin__begin': '_begin',

		'click .admin__begin.team1': '_handleWinTeam1',
		'touchstart .admin__begin.team1': '_handleWinTeam1',

		'click .admin__begin.team2': '_handleWinTeam2',
		'touchstart .admin__begin.team2': '_handleWinTeam2'
	},

	regions: {
		list: '.quiz__main',
		question: '.quiz__question'
	},

	initialize: function () {
		App.currentScore = 0;
	},

	onShow: function () {
		console.log('rendered:', this.model.toJSON());

		var answers = this.model.get('answers'), arr = [];

		_.each(answers, function (item, key) {
			item.number = key;

			arr.push(item);
		}, this);

		var view = new ListView({
			collection: new Backbone.Collection(arr)
		});

		this.list.show(view);

		var questionView = new QuestionView({
			model: new Backbone.Model(this.model.toJSON())
		});

		this.question.show(questionView);

		App.socket.once('win', function () {
			this.update();
		}.bind(this));
	},

	_begin: function () {
		App.socket.emit('begin', {});
	},

	_handleWinTeam1: function () {
		console.log(App.currentScore);

		$.post('/api/question', {
			score: App.currentScore,
			team: 0
		}).success(function () {
			App.currentScore = 0;

			App.socket.emit('win');
		});
	},

	_handleWinTeam2: function () {
		$.post('/api/question', {
			score: App.currentScore,
			team: 1
		}).success(function () {
			App.currentScore = 0;

			App.socket.emit('win');
		});
	},

	/**
	 * Update handler
	 */
	update: function () {
		console.log('update called');

		this.model.fetch({
			// success: function () {
			// 	this.render();
			// }.bind(this)
		});
	},
});

module.exports = Layout;
