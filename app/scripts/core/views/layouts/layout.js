/*
 * AppLayout
 * http://labs.vertic.com
 *
 * Copyright 2015, Vertic A/S
 *
 * @author rkalms <rkalms@vertic.com>
 *
 *
 * NAMING CONVENTIONS:
 * -------------------
 * Singleton-literals and prototype objects:  PascalCase
 *
 * Functions and public variables:            camelCase
 *
 * Global variables and constants:            UPPERCASE
 *
 * Private variables:                         _underscorePrefix
 */

 /*jslint plusplus: true, vars: true, browser: true, white:true*/
/*global require, App*/

'use strict';

var $ = require('jquery'),
	_ = require('underscore'),
	Backbone = require('backbone'),
	Marionette = require('marionette'),

	QuizModel = require('../../models/quiz'),

	// Views
	QuizLayoutAdmin = require('./quiz-admin-layout'),
	QuizLayout = require('./quiz-layout');

var Layout = Marionette.LayoutView.extend({
	el: 'body',

	regions: {
		main: '#feud'
	},

	initialize: function () {
		var quizModel = new QuizModel(this.model.toJSON());

		if (App.admin) {
			this.main.show(new QuizLayoutAdmin({
				model: quizModel
			}));

			quizModel.on('sync', function () {
				this.main.show(new QuizLayoutAdmin({
					model: quizModel
				}));
			}, this);
		} else {
			App.socket.on('begin', function () {
				this.main.show(new QuizLayout({
					model: quizModel
				}));
			}.bind(this));

			quizModel.on('sync', function () {
				this.main.show(new QuizLayout({
					model: quizModel
				}));
			}, this);
		}
	}	
});

module.exports = Layout;
