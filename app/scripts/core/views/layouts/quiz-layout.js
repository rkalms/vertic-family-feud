/*
 * Layout
 * http://labs.vertic.com
 *
 * Copyright 2015, Vertic A/S
 *
 * @author rkalms <rkalms@vertic.com>
 *
 *
 * NAMING CONVENTIONS:
 * -------------------
 * Singleton-literals and prototype objects:  PascalCase
 *
 * Functions and public variables:            camelCase
 *
 * Global variables and constants:            UPPERCASE
 *
 * Private variables:                         _underscorePrefix
 */

 /*jslint plusplus: true, vars: true, browser: true, white:true*/
/*global require, App*/

'use strict';

var $ = require('jquery'),
	_ = require('underscore'),
	Backbone = require('backbone'),
	Marionette = require('marionette'),
	Handlebars = require('handlebars'),

	template = require('../../templates/quiz-template.html'),

	// Views
	ListView = require('../collections/list'),
	QuestionView = require('../question');

var Layout = Marionette.LayoutView.extend({
	template: Handlebars.compile(template),

	regions: {
		list: '.quiz__main',
		question: '.quiz__question'
	},

	initialize: function () {
	},

	onShow: function () {
		console.log('rendered:', this.model.toJSON());

		var answers = this.model.get('answers'), arr = [];

		_.each(answers, function (item, key) {
			item.number = key;

			arr.push(item);
		}, this);

		var view = new ListView({
			collection: new Backbone.Collection(arr)
		});

		this.list.show(view);

		var questionView = new QuestionView({
			model: new Backbone.Model(this.model.toJSON())
		});

		this.question.show(questionView);

		App.socket.once('win', function () {
			this.update();
		}.bind(this));
	},

	/**
	 * Update handler
	 */
	update: function () {
		console.log('update called');

		this.model.fetch();
	}
});

module.exports = Layout;
