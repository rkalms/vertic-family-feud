/*
 * Vertic JS - Site functional wrapper
 * http://labs.vertic.com
 *
 * Copyright 2015, Vertic A/S
 *
 *
 * NAMING CONVENTIONS:
 * -------------------
 * Singleton-literals and prototype objects:  PascalCase
 *
 * Functions and public variables:            camelCase
 *
 * Global variables and constants:            UPPERCASE
 *
 * Private variables:                         _underscorePrefix
 */

/*jslint plusplus: true, vars: true, browser: true, white:true*/
/*global require*/

'use strict';

var $ = require('jquery'),
	_ = require('underscore'),
	Core = require('vertic-core-library'),
	Backbone = require('backbone'),
	Marionette = require('marionette'),
	Handlebars = require('handlebars'),
	Socket = require('socket.io'),

	// Internal
	App = require('./app.js');

window.App = App;

// Document ready
$(function () {
	App.socket = Socket.connect('http://' + window.location.hostname + ':4000');

	// Kick off
	App.start();
});
