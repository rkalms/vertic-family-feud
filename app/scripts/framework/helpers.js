/*
 * Helpers
 *
 * Copyright 2015, Vertic A/S
 *
 *
 * NAMING CONVENTIONS:
 * -------------------
 * Singleton-literals and prototype objects:  PascalCase
 *
 * Functions and public variables:            camelCase
 *
 * Global variables and constants:            UPPERCASE
 *
 * Private variables:                         _underscorePrefix
 */

/*jslint plusplus: true, vars: true, browser: true, white:true*/
/*global require: true*/
var $ = require('jquery'),
	_ = require('underscore'),
	Core = require('vertic-core-library'),
	log = require('log');


var Helpers = Core.extend({
	init: function () {}
});

module.exports = new Helpers();