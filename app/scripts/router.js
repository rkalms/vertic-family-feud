/*
 * Router
 * http://labs.vertic.com
 *
 * Copyright 2012, Vertic A/S
 *
 * @author brasmussen <brasmussen@vertic.com>
 *
 *
 * NAMING CONVENTIONS:
 * -------------------
 * Singleton-literals and prototype objects:  PascalCase
 *
 * Functions and public variables:            camelCase
 *
 * Global variables and constants:            UPPERCASE
 *
 * Private variables:                         _underscorePrefix
 */

 /*jslint plusplus: true, vars: true, browser: true, white:true*/
/*global require: true*/

'use strict';

var $ = require('jquery'),
	_ = require('underscore'),
	Core = require('vertic-core-library'),
	Backbone = require('backbone'),
	Marionette = require('marionette'),
	Handlebars = require('handlebars'),

	// Internal
	Controller = require('./controller.js');


// Router
var Router = Marionette.AppRouter.extend({
	loadModule: function (module) {
		require([module], function (module) {
			module();
			App.trigger('module:loaded');
		});
	},

	loadRouter: function (router, route) {
		require([router], function (Router) {
			var router = new Router(route);

			App.trigger('module:loaded');
		});
	},

	// Add your different routes here
	appRoutes: {
		'': 'page'
	},

	initialize: function () {
		this.controller = new Controller({
			model: this.model
		});
	}
});


module.exports = Router;