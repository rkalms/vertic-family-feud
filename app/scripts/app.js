/*
 * App
 * http://labs.vertic.com
 *
 * Copyright 2012, Vertic A/S
 *
 * @author brasmussen <brasmussen@vertic.com>
 *
 *
 * NAMING CONVENTIONS:
 * -------------------
 * Singleton-literals and prototype objects:  PascalCase
 *
 * Functions and public variables:            camelCase
 *
 * Global variables and constants:            UPPERCASE
 *
 * Private variables:                         _underscorePrefix
 */

 /*jslint plusplus: true, vars: true, browser: true, white:true*/
/*global require */

'use strict';

var $ = require('jquery'),
	_ = require('underscore'),
	Core = require('vertic-core-library'),
	Backbone = require('backbone'),
	Marionette = require('marionette'),
	Handlebars = require('handlebars'),

	// Internal
	Layout = require('./core/views/layouts/layout');

/**
 * App initialization
 */
var App = new Marionette.Application();

// Initializer for App
// Gets attached during application init
// You can add more than one.
App.addInitializer(function () {
	App.on('start', function(){
		App.model = new Backbone.Model();

		App.model.url = '/api/question';

		App.model.fetch();

		App.model.on('sync', function () {
			App.settings = new Backbone.Model();

			App.settings.url = '/api/settings';

			App.settings.fetch();

			App.settings.on('sync', function () {
				App.layout = new Layout({
					model: App.model
				});
			});
		}, this);

		Backbone.history.start();
	},this);
});

module.exports = App;
