/*
 * Controller
 * http://labs.vertic.com
 *
 * Copyright 2012, Vertic A/S
 *
 * @author brasmussen <brasmussen@vertic.com>
 *
 *
 * NAMING CONVENTIONS:
 * -------------------
 * Singleton-literals and prototype objects:  PascalCase
 *
 * Functions and public variables:            camelCase
 *
 * Global variables and constants:            UPPERCASE
 *
 * Private variables:                         _underscorePrefix
 */

 /*jslint plusplus: true, vars: true, browser: true, white:true*/
/*global require: true*/

'use strict';

var $ = require('jquery'),
	_ = require('underscore'),
	Core = require('vertic-core-library'),
	Backbone = require('backbone'),
	Marionette = require('marionette'),
	Handlebars = require('handlebars');


var Controller = Marionette.Controller.extend({
	initialize: function () {
	},

	page: function () {
	}
});

module.exports = Controller;
