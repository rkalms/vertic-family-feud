/*
 * MAIN SERVER
 * http://labs.vertic.com
 *
 * Copyright 2015, Vertic A/S
 *
 *
 * NAMING CONVENTIONS:
 * -------------------
 * Singleton-literals and prototype objects:  PascalCase
 *
 * Functions and public variables:            camelCase
 *
 * Global variables and constants:            UPPERCASE
 *
 * Private variables:                         _underscorePrefix
 */

/*jslint plusplus: true, vars: true, browser: true, white:true*/
/*global require, state*/

'use strict';

var $ = require('jquery'),
	_ = require('underscore'),
	Socket = require('socket.io'),

	// Internal
	App = require('./app.js');

window.App = App;

if (state && state === 'admin') {
	App.admin = true;
}

// Document ready
$(function () {
	App.socket = Socket.connect('http://localhost:4000');

	$('#begin-round').click(function(){
		App.socket.emit('begin', {});
	});

	$('#win-round').click(function(){
		App.socket.emit('win:round', {
			'team': 'some team',
			'score': 245
		});
	});

	$('.admin__end').click(function(){
		App.socket.emit('end', {});
	});


	// Kick off
	App.start();
});