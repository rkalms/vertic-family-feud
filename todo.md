# Database: What to do

Individual auctions where users can bid on projects in realtime. Auctions are timelimited.

* An array of user ID's with their current bids

Users with a simple profile. Name and so on.

## App: What does it need

* Login page (Done)
* List of current auctions (Done)
* Auction page (Done)
* Profile page (Done)

### Auctions

* Add possibility to bid with amount and userid (socket)
* Add timelimit to auction
	- Only show ongoing auctions

### Messages

* User to user instant messaging system (socket)
* Only users who follow each other can IM, except for when an auction finishes (up until deal approved by auctioneer)

### Users

* Add the possibility to follow a user
* Add a social feed for the people you follow, or just generally all new auctions at first (frontpage).

### Profile

* Profile image
* Contact data
* Verified profile
