'use strict';

/**
*
* NAMING CONVENTIONS:
* -------------------
* Singleton-literals and prototype objects:  PascalCase
*
* Functions and public variables:            camelCase
*
* Global variables and constants:            UPPERCASE
*
* Private variables:                         _underscorePrefix
*
*/

var gulp = require('gulp'),
	gutil = require('gulp-util'),
	browserSync = require('browser-sync'),
	browserify = require('browserify'),
	source = require('vinyl-source-stream'),
	buffer = require('vinyl-buffer'),
	transform = require('vinyl-transform'),
	stringify = require('stringify');

var watchify = require('watchify'),
	debowerify = require('debowerify');

var server = require('gulp-express');

var $ = require('gulp-load-plugins')(),
	_ = { app: 'app', dist: 'dist', reload: browserSync.reload };

// ✓ styles
gulp.task('styles', function () {
	return $.rubySass(_.app + '/styles/style.scss', {
		style: 'expanded',
		compass: true
	})
	.pipe(gulp.dest(_.dist + '/styles'))
	.pipe(_.reload({ stream: true }));
});
// /end styles

// ✓ browserify
var bundler = watchify(browserify('./' + _.app + '/scripts/main.js', {
	paths: ['./node_modules', './app']
}), watchify.args);

var bundlerServ = watchify(browserify('./' + _.app + '/scripts/main-server.js', {
	paths: ['./node_modules', './app']
}), watchify.args);

gulp.task('browserify', function () {
	function bundleClient() {
		return bundler.bundle()
			// log errors if they happen
			.on('error', gutil.log.bind(gutil, 'Browserify Error'))
			.pipe(source('bundle.js'))
			// optional, remove if you dont want sourcemaps
			.pipe(buffer())
			.pipe($.sourcemaps.init({ loadMaps: true })) // loads map from browserify file
			.pipe($.sourcemaps.write('./')) // writes .map file
			//
			.pipe(gulp.dest(_.dist + '/scripts'));
	}

	function bundleServer() {
		return bundlerServ.bundle()
			// log errors if they happen
			.on('error', gutil.log.bind(gutil, 'Browserify Error'))
			.pipe(source('bundle-server.js'))
			// optional, remove if you dont want sourcemaps
			.pipe(buffer())
			.pipe($.sourcemaps.init({ loadMaps: true })) // loads map from browserify file
			.pipe($.sourcemaps.write('./')) // writes .map file
			//
			.pipe(gulp.dest(_.dist + '/scripts'));
	}

	bundler
		// Testing browserify-shim, therefore this is uncommented
		// .transform(debowerify)
		.transform(stringify(['.html']))

		// Output build logs to terminal
		.on('log', gutil.log)
		.on('update', function () {
			bundleClient();
		});

	bundlerServ
		// Testing browserify-shim, therefore this is uncommented
		// .transform(debowerify)
		.transform(stringify(['.html']))

		// Output build logs to terminal
		.on('log', gutil.log)
		.on('update', function () {
			bundleServer();
		});

	return (function() {
		bundleClient();
		bundleServer();
	}());
});
// /end browserify

// ✓ jshint
gulp.task('jshint', function () {
	return gulp.src([
			_.app + '/scripts/**/*.js',
			'!' + _.app + '/scripts/vendor/**/*.js'
		])
		.pipe($.jshint())
		.pipe($.jshint.reporter('jshint-stylish'))
		.pipe($.jshint.reporter('fail'));
});
// /end jshint

// ✓ html
gulp.task('html', ['styles'], function () {
	var lazypipe = require('lazypipe');
	var cssChannel = lazypipe()
		.pipe($.csso)
		.pipe($.replace, 'fonts');

	var assets = $.useref.assets({ searchPath: '{.tmp, app}' });

	return gulp.src(_.app + '/**/*.html')
		.pipe(assets)
		.pipe($.if('*.js', $.uglify()))
		.pipe($.if('*.css', cssChannel()))
		.pipe(assets.restore())
		.pipe($.useref())
		//.pipe($.if('*.html', $.minifyHtml({ conditionals: true, loose: true })))
		.pipe(gulp.dest(_.dist));
});
// /end html

// ✓ images
gulp.task('images', function () {
	return gulp.src(_.app + '/assets/images/**/*')
		.pipe($.cache($.imagemin({
			progressive: true,
			interlaced: true
		})))
		.pipe(gulp.dest(_.dist + '/assets/images'));
});
// /end images

// ✓ watch
gulp.task('watch', ['styles', 'browserify', 'nodemon'], function () {
	var port = process.env.PORT || 4000;

	browserSync({
		proxy: 'http://localhost:' + port,
		// We're using Google Chrome as standard @ Vertic
		browser: 'google chrome',
		files: [_.dist + '/scripts/bundle.js']
	});

	gulp.watch(_.app + '/styles/**/*.scss', ['styles']);
});

gulp.task('nodemon', function (cb) {
	var called = false;

	return $.nodemon({
		script: 'server/server.js',
		ext: 'js html',
		watch: ['server/**/*.js', '**/*.html'],
		ignore: ['node_modules/**/*']
	})
	.on('start', function () {
		if (!called) {
			cb();
		}
		called = true;
	})
	.on('restart', function () {
		setTimeout(function () {
			_.reload();
		}, 500);
	});
});
// /end watch

// ✓ compile handlebars
gulp.task('hbs', ['html'], function () {
	var options = {
		batch : ['./app/partials']
	}

	var assets = $.useref.assets({ searchPath: '{.tmp, app}' });

	return gulp.src('./app/templates/**/*.html')
		.pipe($.compileHandlebars({}, options))
		.pipe($.useref())
		.pipe(gulp.dest('./dist/built-templates'));
});
// /end handlebars

gulp.task('fonts', function () {
	gulp.src(_.app + '/assets/fonts/**/*')
		.pipe(gulp.dest(_.dist + '/assets/fonts'));
});

gulp.task('modernizr', function () {
	gulp.src(_.app + '/scripts/**/*.js')
		.pipe($.modernizr('modern.js'))
		.pipe(gulp.dest(_.dist + '/scripts/'));
});

gulp.task('clean', require('del').bind(null, ['.tmp', 'dist']));

gulp.task('build', ['modernizr', 'browserify', 'images', 'fonts', 'hbs'], function () {
	return gulp.src('dist/**/*')
		.pipe($.size({ title: 'build', gzip: true }))
		.once('end', function () {
			process.exit();
		});
});

gulp.task('default', ['watch']);
