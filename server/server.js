'use strict';
 
// Simple express server
var express 		= require('express'),
	fs 				= require('fs'),
	hbs 			= require('hbs'),
	bodyParser 		= require('body-parser'),
	cookieParser	= require('cookie-parser'),
	mongoose 		= require('mongoose');

var environment 	= process.env.environment || 'dev',
	db 				= require('./config/db')(environment);

var app 			= express(),
	http 			= require('http').Server(app),
	io 				= require('socket.io').listen(http),
	session 		= require('express-session'),
	passport		= require('passport'),
	flash			= require('connect-flash'),
	morgan			= require('morgan'),
	sockets			= require('./sockets');

var MongoStore		= require('connect-mongo')(session);

var router 			= express.Router(),
	routes 			= require('./routes');

var partialsDir 	= './app/partials',
	filenames 		= fs.readdirSync(partialsDir);

// Connect to db
mongoose.connect(db.connection);

/**
* Handlebars setup
*/
app.set('view engine', 'html');
app.engine('html', hbs.__express);

app.set('views', 'app/views');

/**
* Config
*/

// Configure passport
require('./config/passport')(passport);

app.use(express.static('app'), express.static('dist'));

app.use(morgan('dev')); // log every request to the console
app.use(cookieParser()); // parse cookies

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(session({
	secret: 'creepyauctionairysecret',
	saveUninitialized: true,
	resave: true,
	store: new MongoStore({ mongooseConnection: mongoose.connection })
}));

app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions
app.use(flash()); // use connect-flash for flash messages stored in session

// Register partials
filenames.forEach(function (filename) {
	var matches = /^([^.]+).html$/.exec(filename);

	if (!matches) {
		return;
	}

	var name = matches[1];
	var template = fs.readFileSync(partialsDir + '/' + filename, 'utf8');

	hbs.registerPartial(name, template);
});

/**
* Routes
*/
routes(app, io, passport, db);
sockets(io);
 
http.listen(4000);
