/**
* API user routes
*
* @author rkalms <rkalms@gmail.com>
*/

'use strict';

var fs = require('fs');
var dir = process.env.appvirtdir || '/',

	// Models
	UserModel = require('../models/user');

module.exports = function (app, io, passport, config) {
	// Create new user
	app.post('/api/users', function (req, res) {
		var user = new UserModel({
			username: req.body.username,
			password: req.body.password
		});

		user.save(function(err) {
			if (err) res.send(err);

			res.json({ message: 'New user has been added!' });
		});
	});

	// Get all users
	app.get('/api/users', function (req, res) {
		UserModel.find(function(err, users) {
			if (err) res.send(err);

			res.json(users);
		});
	});
};
