/**
* Routes
* 	Routing wrapper.
*
* @author rkalms <rkalms@gmail.com>
*/

'use strict';

var fs = require('fs');
var dir = process.env.appvirtdir || '/',

	// Routes
	api = require('./api'),
	web = require('./web'),
	user = require('./api-user');

/**
* readJsonFileSync()
*
* @param {String} Filepath
* @param {String} Encoding (optional)
*/
function readJsonFileSync(filepath, encoding){
	if (typeof (encoding) == 'undefined'){
		encoding = 'utf8';
	}

	var file = fs.readFileSync(filepath, encoding);

	return JSON.parse(file);
}

module.exports = function (app, io, passport, db) {
	// Enable different routes
	api(app, io, passport, db);
	web(app, io, passport, db);
	user(app, io, passport, db);

	app.all(dir + '*', function (req, res) {
		res.sendStatus(404);
	});
};

