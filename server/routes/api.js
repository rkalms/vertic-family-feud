/**
* API routes
*
* @author rkalms <rkalms@gmail.com>
*/

'use strict';

var fs 		= require('fs');
var dir 	= process.env.appvirtdir || '/',
	auth 	= require('../controllers/auth'),

	// Models
	Question = require('../models/question'),
	Settings = require('../models/settings');

/**
* readJsonFileSync()
*
* @param {String} Filepath
* @param {String} Encoding (optional)
*/
function readJsonFileSync(filepath, encoding){
	if (typeof (encoding) == 'undefined'){
		encoding = 'utf8';
	}

	var file = fs.readFileSync(filepath, encoding);

	return JSON.parse(file);
}

module.exports = function (app, io, passport, config) {
	app.get('/api', function (req, res) {
		res.json({ message: 'API running...' });
	});

	// Get all questions
	app.get('/api/question', function (req, res) {
		Question.findOne({ current: true }, function (err, question) {
			if (err) res.send(err);

			Settings.findOne({ name: 'quiz' }, function (err, setting) {
				question.teams = setting.teams;

				question.save(function (err, newQuestion) {
					res.json(newQuestion);
				});
			});
		});
	});

	app.get('/api/settings', function (req, res) {
		Settings.findOne({ name: 'quiz' }, function (err, settings) {
			if (err) res.send(err);

			res.json(settings);
		});
	});

	// Get next question / round ends
	app.post('/api/question', function (req, res) {
		Question.findOne({ current: true }, function (err, question) {
			if (err) res.send(err);

			var body = req.body;

			if (!question) {
				res.json({ outcome: 'Could not find question.' });

				return;
			}

			// Update team score
			Settings.findOne({ name: 'quiz' }, function (err, setting) {
				if (err) res.send(err);

				var team = setting.teams[body.team];

				if (team) {
					team.score = Number(team.score) + Number(body.score);
				}

				setting.save(function (err, newSetting) {
					Question.findOne({_id: { $gt: question._id }}, function (err, newQuestion) {
						if (err) res.send(err);

						if (!newQuestion) {
							res.json({
								outcome: 'No more questions!'
							});
						}

						question.current = false;

						question.teams = setting.teams;

						question.save(function (err) {
							if (err) return req.json({ outcome: 'Could not save question.' });

							newQuestion.current = true;

							// Set teams on current model
							newQuestion.teams = newSetting.teams;

							newQuestion.save(function (err) {
								if (err) return req.json({ outcome: 'Could not save new question.' });

								console.log(newQuestion);
								
								res.json(newQuestion);
							});
						});
					});
				});
			});
		});
	});
};
