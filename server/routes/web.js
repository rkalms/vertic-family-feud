/**
* Web routes
*
* @author rkalms <rkalms@gmail.com>
*/

'use strict';

function isLoggedIn(req, res, next) {
	if (req.isAuthenticated()) {
		return next();
	}

	res.redirect('/user/signup');
}

module.exports = function(app, io, passport){
	// Serve index.html
	app.get('/', function (req, res) {
		res.render('index', {
			questions: {}
		});
	});

	app.get('/server', function (req, res) {
		res.render('index-server', {
			questions: {}
		});
	});

	// User
	app.get('/user/login', function (req, res) {
		var data = {
			message: req.flash('loginMessage')
		};

		console.log(req.flash());

		res.render('user-login', data);
	});

	app.post('/user/login', passport.authenticate('local-login', {
		successRedirect : '/user/profile',
		failureRedirect : '/user/login',
		failureFlash : true // allow flash messages
	}));

	app.get('/user/signup', function (req, res) {
		var data = {
			message: req.flash('signupMessage')
		};

		res.render('user-signup', data);
	});

	app.post('/user/signup', passport.authenticate('local-signup', {
		successRedirect : '/user/profile',
		failureRedirect : '/user/signup',
		failureFlash : true // allow flash messages
	}));

	app.get('/user/profile', isLoggedIn, function (req, res) {
		var data = {
			user: req.user
		};

		res.render('user', data);
	});
};
