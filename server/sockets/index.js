/**
 * Socket handler
 *
 */

'use strict';

var flipped = {};

module.exports = function (io) {

	io.sockets.on('connection', function (socket) {
		// listen to incoming bids
		socket.on('flip', function (content) {
			console.log('flip received', content, flipped);

			if (flipped.content) {
				flipped.content = false;
			} else {
				flipped.content = true;
			}

			// echo to the sender
			socket.emit('flip', content.index);

			// broadcast the flip to all clients
			socket.broadcast.emit('flip', content.index);
		});

		socket.on('begin', function () {
			console.log('begin received');

			// echo to the sender
			socket.emit('begin');

			socket.broadcast.emit('begin');
		});

		socket.on('win', function () {
			console.log('round win received');

			// echo to the sender
			socket.emit('win');

			socket.broadcast.emit('win');
		});
	});
};