// Load required packages
var mongoose = require('mongoose');

// Define our auction schema
var SettingsSchema = new mongoose.Schema({
	name: String,
	active: Boolean,
	teams: [
		{
			name: String,
			score: { type: Number, default: 0 }
		}
	]
});

// Export the Mongoose model
module.exports = mongoose.model('Settings', SettingsSchema);
