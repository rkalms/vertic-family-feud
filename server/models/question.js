// Load required packages
var mongoose = require('mongoose');

// Define our auction schema
var QuestionSchema = new mongoose.Schema({
	question: String,
	answers: mongoose.Schema.Types.Mixed,
	current: { type: Boolean, default: false },
	teams: mongoose.Schema.Types.Mixed
});

// Export the Mongoose model
module.exports = mongoose.model('Question', QuestionSchema);
